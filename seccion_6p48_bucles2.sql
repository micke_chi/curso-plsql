/*
Practica2 Poner frase al reves
Practica3 Si en el texto aparece el car�cter "x" debe salir del bucle
*/
/*
SET SERVEROUTPUT ON

DECLARE

    TEXTO VARCHAR2(100);
    i PLS_INTEGER;
    TEXTO_REVERSE VARCHAR2(100);
    CARACTER VARCHAR2(1);

BEGIN
    TEXTO := 'Hola mundo';
    i := LENGTH(TEXTO);
    
    WHILE i > 0 LOOP
        CARACTER := SUBSTR(TEXTO, i, 1);
        TEXTO_REVERSE := TEXTO_REVERSE || CARACTER;
        i := i - 1;
        EXIT WHEN LOWER(CARACTER) = 'x';
    END LOOP;
    
    DBMS_OUTPUT.PUT_LINE('El texto al reves: '||TEXTO_REVERSE);
    
END;
*/


/*
Practica4 reemplazar los caracteres de una cadena por asteriscos
*/
/*
SET SERVEROUTPUT ON

DECLARE

    NOMBRE VARCHAR2(100);
    CARACTERES VARCHAR2(100);
    
BEGIN
    NOMBRE := 'Hola mundo';
    
    FOR i IN 1..LENGTH(NOMBRE) LOOP
        CARACTERES := CARACTERES||'*';
    END LOOP;
    
    DBMS_OUTPUT.PUT_LINE('Texto reemplazado con asteriscos: '||CARACTERES);
    
END;
*/


SET SERVEROUTPUT ON

DECLARE

    INICIO NUMBER;
    FIN NUMBER;
    
BEGIN
    INICIO := 1;
    FIN := 50;
    
    FOR i IN INICIO..FIN LOOP
        IF (MOD(i, 4) = 0) THEN
            DBMS_OUTPUT.PUT_LINE(i);
        END IF;
    END LOOP;    
END;



