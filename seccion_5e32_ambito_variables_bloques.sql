SET SERVEROUTPUT ON
DECLARE
    X NUMBER := 150; --Global
    Z NUMBER := 40; --Global
BEGIN
    DBMS_OUTPUT.PUT_LINE('Este es el bloque de nivel 1');
    DBMS_OUTPUT.PUT_LINE('Valor de X en nivel 1: ' || X);
    DECLARE
        X NUMBER := 100; --Local
        Z NUMBER := 20; --Local
        Y NUMBER := 10; --Local
    BEGIN
        DBMS_OUTPUT.PUT_LINE('Este es el bloque de nivel 2');
        DBMS_OUTPUT.PUT_LINE('Valor de X en nivel 2: ' || X);
        DBMS_OUTPUT.PUT_LINE('Valor de Z en nivel 2: ' || Z);
    END;
    DBMS_OUTPUT.PUT_LINE('Valor de Z en nivel 1: ' || Z);
    -- La siguiente instrucci�n muestra un error ya la variable "Y" con esta en el alcance del bloque de nivel 1, esto por que "Y"
    -- esta declarado en un bloque �rjodo
    --DBMS_OUTPUT.PUT_LINE('Valor de Y en nivel 1: ' || Y);

END;