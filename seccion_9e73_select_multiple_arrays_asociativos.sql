/*
Como manejar m�s de una fila con los arrays asociativos, seleccionar mas de una fila de la tabla a la avez y agregarlo a el array,
esto no es recomendable de hacer con datos grandes ya que esta operacion ocuparia mucha memoria; para esos casos se usa cursores
*/

SET SERVEROUT ON

DECLARE
    TYPE DEPARTAMENTOS IS TABLE OF
    departments%ROWTYPE
    INDEX BY PLS_INTEGER;
    
    DEPTOS DEPARTAMENTOS;
BEGIN
    -- Obtengo los primeros 10 registros de la tabla d departamentos
    FOR i IN 1..10 LOOP
        --Voy agregando los registros al array asociativo
        SELECT * INTO DEPTOS(i) FROM departments WHERE department_id = (i*10);
    END LOOP;
    
    -- Recorro el array con un for y tomamos como limites los indices primero y ultimo con os metodos FIRST Y LAST
    FOR i IN DEPTOS.FIRST..DEPTOS.LAST LOOP
        DBMS_OUTPUT.PUT_LINE(DEPTOS(i).department_name);
    END LOOP;
    
END;
