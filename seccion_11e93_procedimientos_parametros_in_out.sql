/*
Los parámetros tipo IN-OUT se usa para devolver un valor desde dentro del procedimiento, si cambio el valor de la variable
dentro del procedimiento tambien cambiara el valor dentro de quien invoco al procedimiento
*/

create or replace PROCEDURE CALCULAR_IMPUESTO_OUT (
    EMPLEADO_ID IN EMPLOYEES.EMPLOYEE_ID%TYPE,
    T1 IN OUT NUMBER)
IS
    PSALARY NUMBER :=0;
    
BEGIN
    IF T1 < 0 OR T1 > 60 THEN
        RAISE_APPLICATION_ERROR(-2000, 'El porcentaje debe estar entr 0 y 60');
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('TAX IN PROCEDURE: ' || T1);
    
    SELECT SALARY INTO PSALARY FROM EMPLOYEES WHERE EMPLOYEE_ID = EMPLEADO_ID;
    T1 := PSALARY*T1/100;
    DBMS_OUTPUT.PUT_LINE('SALARY ' || PSALARY);
    
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('No existe el empleado');
END;