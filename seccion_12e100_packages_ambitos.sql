/*
PAQUETES: Los paquetes son como librerias en sql
- Los paquetes estan conformados por 2 secciones:
-- Especificaciones: es el bloque en el cual declaramos todas aquellas variables y declaraciones publicas, que seran expuestas 
por el paquete o mejor dicho es la declaración de todo aquello que podra ser usado al invocar al paquete. Toto lo que se 
declarado en "Especificaciones" tiene que programarse. Siempre es obligatorio en el paquete.
-- Body: El cuerpo del paquete es la parte privada o encapsulada del paquete, en el se pueden encontrar variables, codigo, 
procedimientos, funciones que pudieron haber sido declaradas o no en las Especificaciones.No es obligatorio en el paquete.

- El valor de las variables publicas en un package de solo se resetean cuando se cierra la sesion, osea que permanecen con el
valor que le pongamos en la sesion

*/

CREATE OR REPLACE PACKAGE MIPACKAGE
IS
    VAR1 NUMBER := 10;
    VAR2 VARCHAR2(100);
END;
/

SET SERVEROUTPUT ON
DECLARE
BEGIN
    MIPACKAGE.VAR1 := MIPACKAGE.VAR1 + 20;
    DBMS_OUTPUT.PUT_LINE('Var1: ' || mipackage.VAR1);
END;