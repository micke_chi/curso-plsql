/*
*Funciones son muy similares a las funciones convencionales
- siempre retorna un tipo de valor. Si declare que la funcion retorna NUMBER, entonces eso tiene que cumplirse
- antes del bloque IS tengo que declarar que tipo de valor retornare
Restricciones al usar funciones directamente con comendos sql:
- solo puedo usar parametros IN
- debe existir en la misma DB
- debe devolder sql o pl/sql
*/

CREATE OR REPLACE FUNCTION F_CALCULAR_IMPUESTO(
    EMPL_ID IN EMPLOYEES.EMPLOYEE_ID%TYPE,
    PORCENTAJE IN NUMBER
)

RETURN NUMBER
IS 
    IMPUESTO NUMBER := 0;
    PSALARY NUMBER := 0;
BEGIN
    IF PORCENTAJE < 0 OR PORCENTAJE > 60 THEN
        RAISE_APPLICATION_ERROR(-20000, 'EL PORCENTAJE DEBE ESTAR ENTRE 0 Y 60');
    END IF;
    SELECT SALARY INTO PSALARY FROM EMPLOYEES WHERE EMPLOYEE_ID = EMPL_ID;
    
    IMPUESTO := (PSALARY * PORCENTAJE / 100);
    
    RETURN IMPUESTO;
    
EXCEPTION
    WHEN NO_DATA_FOUND THEN 
        DBMS_OUTPUT.PUT_LINE('NO EXISTE EL EMPLEADO');
END;

-- Ejecutando la funci�n 
SET SERVEROUTPUT ON
DECLARE
    EMPL_ID EMPLOYEES.EMPLOYEE_ID%TYPE;
    PORCENTAJE NUMBER;
    PORCENTAJE_NUMERICO NUMBER;
BEGIN
    EMPL_ID := 100;
    PORCENTAJE := 10;
    PORCENTAJE_NUMERICO := f_calcular_impuesto(EMPL_ID, PORCENTAJE);
    DBMS_OUTPUT.PUT_LINE('El porcentaje calculado: ' || PORCENTAJE_NUMERICO);
END;

/*
*Tambi�n es posible llamar funciones desde comandos sql puros 
*/
SELECT FIRST_NAME, SALARY, f_calcular_impuesto(EMPLOYEE_ID, 10) IMPUESTO FROM EMPLOYEES;








