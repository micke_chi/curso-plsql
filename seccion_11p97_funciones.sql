/*
*Practica 1: Crear una funci�n que tenga como par�metro un n�mero de departamento y que devuelve la suma de los salarios de dicho 
departamento. La imprimimos por pantalla.
- Si el departamento no existe debemos generar una excepci�n con dicho mensaje
*/

/*
CREATE OR REPLACE FUNCTION F_SUMA_SALARIOS_DEPARTAMENTOS(
    DEPTO_ID DEPARTMENTS.DEPARTMENT_ID%TYPE
)
RETURN NUMBER
IS
    DEPTO DEPARTMENTS%ROWTYPE;
    TOTAL_SALARIOS NUMBER := 0;
    CURSOR CDEPTO(PDEPTO_ID DEPARTMENTS.DEPARTMENT_ID%TYPE) IS SELECT * FROM DEPARTMENTS WHERE DEPARTMENT_ID = PDEPTO_ID;
BEGIN
    OPEN CDEPTO(DEPTO_ID);
        FETCH CDEPTO INTO DEPTO;
        IF(CDEPTO%FOUND) THEN
            DBMS_OUTPUT.PUT_LINE('EL DEPARTAMENTO ES: ' || DEPTO.department_name);
            SELECT SUM(SALARY) INTO TOTAL_SALARIOS FROM EMPLOYEES WHERE DEPARTMENT_ID = DEPTO_ID;
            RETURN TOTAL_SALARIOS;
        ELSE
            RAISE_APPLICATION_ERROR(-20100, 'EL DEPARTAMENTO NO EXISTE');
        END IF;
    CLOSE CDEPTO;
END;

-- Ejecutamos funcion
SET SERVEROUTPUT ON
DECLARE
    DEPTO_ID DEPARTMENTS.DEPARTMENT_ID%TYPE;
    SALARIO_DEPTO NUMBER;
BEGIN
    DEPTO_ID := 80;
    SALARIO_DEPTO := f_suma_salarios_departamentos(DEPTO_ID);
    DBMS_OUTPUT.PUT_LINE('LA SUMA DE SALARIOS DEL DEPARTAMENTO ' || DEPTO_ID || ': ' || SALARIO_DEPTO);
END;
*/


/*
Practica 2: Modificar el programa anterior para incluir un par�metro de tipo OUT por el que vaya el n�mero de empleados afectados por la 
query. Debe ser visualizada en el programa que llama a la funci�n. De esta forma vemos que se puede usar este tipo de par�metros
tambi�n en una funci�n
*/

/*
CREATE OR REPLACE FUNCTION F_SUMA_SALARIOS_DEPTOS_P2(
    DEPTO_ID DEPARTMENTS.DEPARTMENT_ID%TYPE,
    EMPLEADOS_DEPTO OUT NUMBER
)
RETURN NUMBER
IS
    DEPTO DEPARTMENTS%ROWTYPE;
    TOTAL_SALARIOS NUMBER := 0;
    CURSOR CDEPTO(PDEPTO_ID DEPARTMENTS.DEPARTMENT_ID%TYPE) IS SELECT * FROM DEPARTMENTS WHERE DEPARTMENT_ID = PDEPTO_ID;
BEGIN
    OPEN CDEPTO(DEPTO_ID);
        FETCH CDEPTO INTO DEPTO;
        IF(CDEPTO%FOUND) THEN
            DBMS_OUTPUT.PUT_LINE('EL DEPARTAMENTO ES: ' || DEPTO.department_name);
            SELECT SUM(SALARY), COUNT(*) CANTIDAD_EMPL INTO TOTAL_SALARIOS, EMPLEADOS_DEPTO FROM EMPLOYEES WHERE DEPARTMENT_ID = DEPTO_ID;
            RETURN TOTAL_SALARIOS;
        ELSE
            RAISE_APPLICATION_ERROR(-20100, 'EL DEPARTAMENTO NO EXISTE');
        END IF;
    CLOSE CDEPTO;
END;

-- Ejecutamos funcion
SET SERVEROUTPUT ON
DECLARE
    DEPTO_ID DEPARTMENTS.DEPARTMENT_ID%TYPE;
    SALARIO_DEPTO NUMBER;
    EMPLEADOS_DEPTO NUMBER;
BEGIN
    DEPTO_ID := 80;
    EMPLEADOS_DEPTO := 0;
    SALARIO_DEPTO := f_suma_salarios_deptos_p2(DEPTO_ID, EMPLEADOS_DEPTO);
    DBMS_OUTPUT.PUT_LINE('LA SUMA DE SALARIOS DEL DEPARTAMENTO ' || DEPTO_ID || ': ' || SALARIO_DEPTO);
    DBMS_OUTPUT.PUT_LINE('EMPLEADOS DEL DEPARTAMENTO ' || EMPLEADOS_DEPTO);
END;

*/

/*
*Practica 3: Crear una funci�n llamada CREAR_REGION, donde se pase un nombre de regi�n que debe ser insertada en la tabla REGIONS y que 
devuelva un n�mero. De forma autom�tica debe calcular el c�digo de regi�n m�s alto, a�adir 1 e insertar un registro con el nuevo
n�mero y la regi�n que se ha pasado.
- Si la regi�n no existe debe arrojar un error de control.
- El valor devuelto es el n�mero que ha asignado a la regi�n
- Las soluciones est�n en el fichero adjunto
*/

CREATE OR REPLACE FUNCTION CREAR_REGION(NOMBRE_REGION VARCHAR2)
RETURN NUMBER
IS
    MAX_CODE NUMBER;
    REGION_EXISTE REGIONS%ROWTYPE;
BEGIN
    
    SELECT * INTO REGION_EXISTE FROM REGIONS WHERE REGION_NAME = NOMBRE_REGION;
    RAISE_APPLICATION_ERROR(-20101, 'Ya existe una region con este nombre');
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        SELECT (MAX(REGION_ID) + 1) INTO MAX_CODE FROM REGIONS;
        INSERT INTO REGIONS VALUES(MAX_CODE, NOMBRE_REGION);
        COMMIT;
        RETURN MAX_CODE;
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Ocurrio un error en la funcion: ' || SQLCODE || '-' || SQLERRM);
END;

-- Ejecuci�n de la funci�n

SET SERVEROUTPUT ON

DECLARE
    NOMBRE_REGION_NUEVA VARCHAR2(50);
    REGION_ID_NUEVA NUMBER;
BEGIN
    NOMBRE_REGION_NUEVA := 'Mexico';
    REGION_ID_NUEVA := crear_region(NOMBRE_REGION_NUEVA);
    DBMS_OUTPUT.PUT_LINE('EL ID DE LA NUEVA REGION: ' || REGION_ID_NUEVA);
END;






    
