-- Instancia de BD: Por cada instancia de oracle se tiene una BD, cada instancia consumo muchos recursos por lo que se recomienda
-- una sola instancia por servidor. Una instancia de Oracle solo puede abrir una sola BD a la vez. As� mismo una instancia esta
-- compuesta de:

-- * System Global Area (SGA): �rea de memeoria compartida que se utiliza para el intercambio de datos entre el servidor y las
-- aplicaciones cliente
-- * * Library Cache: Almacena las sentencias SQL m�s recientes en memoria
-- * * Database Dictionary Cache: Buffer para el diccionario de datos. Tablas, columnas, tipos, �ndices.
-- * * Database Buffer Cache: Buffer de la base de datos, contiene bloques de datos que han sido cargados desde los Data File
-- * * Redo Log Buffer Cache: Bloques de datos que han sido actualizados

-- * Procesoa de fondo: 
-- * * PMON Process Monior: Monitorea los procesos de los usuarios, en caso que la conexion falle.
-- * * SMON System Monior: Se encarga de recuperar la instancia y abrir la BD, en caso de que ocurra una falla.
-- * * CKPT CheckPoint Process: Sintoniza las tareas de grabacion en la BD.
-- * * DBWR Database Writer: Escribe los bloques de datos de la memoria a la BD.
-- * * LGWR Log Writer: Graba los bloques del Redo Log del buffer a los archivos Redo Log File.

-- El componente basico es el bloque, el cual se divide en: 
-- ** DECLARE
-- ** BEGGIN
-- ** EXCEPTION
-- ** END

-- Existen tipos de componentes PL SQL:
-- ** Bloques anonimos
-- ** Procedimientos almacenados y funciones
-- ** Triggers

-- BLOQUES ANONIMOS

--Se utiliza para visualizar salida por pantalla
SET SERVEROUTPUT ON
BEGIN
DBMS_OUTPUT.PUT_LINE(100);
DBMS_OUTPUT.PUT_LINE('Hola '||' Mundo');
END;