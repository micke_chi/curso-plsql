SET SERVEROUTPUT ON

DECLARE
    I VARCHAR2(100) := 'AAAA';
BEGIN
-- Este es un loop for normal
/*    FOR i IN 1..10 LOOP
        DBMS_OUTPUT.PUT_LINE('Iteración: ' i);
    END LOOP;
*/
-- Este es un loop for reverse, itera de forma descendente
    FOR i IN REVERSE 1..10 LOOP --PLS_INTEGER
        DBMS_OUTPUT.PUT_LINE('Iteración: ' || i);
    END LOOP;

END;