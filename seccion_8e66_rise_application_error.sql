/*
Usando el comando 'RAISE_APPLICATION_ERROR': devolver un error personalizado y abortar un programa. El codigo de error debe estar entre -20000 y -20999
*/

SET SERVEROUTPUT ON
DECLARE
    NUMERO_REGION NUMBER;
    NOMBRE_REGION VARCHAR2(100);
BEGIN
    NUMERO_REGION := 98;
    NOMBRE_REGION := 'Namecu';
    
    IF NUMERO_REGION > 100 THEN
        RAISE_APPLICATION_ERROR(-20001, 'El id no puede ser mayor a 100');
    ELSE
        INSERT INTO hr.regions (region_id, region_name) VALUES (NUMERO_REGION, NOMBRE_REGION);
        COMMIT;
    END IF;
END;