/*
Usar funciones de un paquete dentro de comandos sql
*/
CREATE OR REPLACE PACKAGE MIPACK3
IS 
    FUNCTION F_CONVERT_UPLO( NAME VARCHAR2, CONVERSION_TYPE CHAR) RETURN VARCHAR2;
END;
/

CREATE OR REPLACE PACKAGE BODY MIPACK3
IS
    -- Funciones privadas, solo accesibles dentro del paquete
    FUNCTION TO_UPPER(NAME VARCHAR)
    RETURN VARCHAR2
    IS
    BEGIN
        RETURN UPPER(NAME);
    END TO_UPPER;
    -- Funciones privadas, solo accesible dentro del paquete
    FUNCTION TO_LOWER(NAME VARCHAR2)
    RETURN VARCHAR2
    IS
    BEGIN
        RETURN LOWER(NAME);
    END TO_LOWER;
    
    -- Procedimiento publico ya que fue declara en la cabecera o especificaciones
    FUNCTION F_CONVERT_UPLO(NAME VARCHAR2, CONVERSION_TYPE CHAR)
    RETURN VARCHAR2
    IS
    BEGIN
        IF (CONVERSION_TYPE = 'U') THEN
            RETURN TO_UPPER(NAME);
        ELSIF (CONVERSION_TYPE = 'L') THEN
            RETURN TO_LOWER(NAME);
        ELSE
            RAISE_APPLICATION_ERROR(-20110, 'El tipo de conversión debe ser U o L');
        END IF;
    END F_CONVERT_UPLO;
END MIPACK3;

/

SET SERVEROUTPUT ON
DECLARE
    NOMBRE_CONVERTIDO VARCHAR2(100);
BEGIN
    NOMBRE_CONVERTIDO := mipack3.f_convert_uplo('mIGUEL aNGEl chI pECh', 'U');
    DBMS_OUTPUT.PUT_LINE('El nombre convertido:' || NOMBRE_CONVERTIDO);
    
END;

/

-- Es posible llamar a las funciones dentro de un paquete desde una comando sql
SELECT FIRST_NAME, mipack3.f_convert_uplo(FIRST_NAME, 'U') UPPER_NAME FROM EMPLOYEES;
