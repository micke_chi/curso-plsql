/*
Colecciones y tipos compuestos
- Son componentes que pueden albergar multiples valores, a diferencia de los escalare que solo contienen un valor, estos son:
    - Records: como Rowtype, como un fila de datos de distintos tipos. Es posible crear Rowtypes(tipos compuestos) 
    personalizados a esto se le llama un Records

*/

SET SERVEROUTPUT ON

DECLARE
    TYPE empleado IS RECORD (
    nombre VARCHAR2(100),
    salario NUMBER,
    fecha employees.hire_date%TYPE,
    datos employees%ROWTYPE
    );
    
    EMPLEADO1 empleado;

BEGIN
    SELECT * INTO EMPLEADO1.DATOS
    FROM employees WHERE employee_id = 100;
    
    EMPLEADO1.nombre := EMPLEADO1.datos.first_name || ' ' || EMPLEADO1.datos.last_name;
    EMPLEADO1.salario := EMPLEADO1.datos.salary * 1.16;
    EMPLEADO1.fecha := EMPLEADO1.datos.hire_date;
    
    DBMS_OUTPUT.PUT_LINE(EMPLEADO1.nombre);
    DBMS_OUTPUT.PUT_LINE(EMPLEADO1.salario);
    DBMS_OUTPUT.PUT_LINE(EMPLEADO1.fecha);
    DBMS_OUTPUT.PUT_LINE(EMPLEADO1.datos.phone_number);

END;