/**
* Uso de la clausula WHEN para que el Trigger solo se dispare en ciertas condiciones
*/

CREATE OR REPLACE TRIGGER TR_REGION6
BEFORE INSERT OR UPDATE OF REGION_NAME OR DELETE 
ON REGIONS
-- Con esto el indicamos que el trigger se debe ejecutar por cada fila
FOR EACH ROW
-- Condición WHEN que no permite iniciar el trigger si el valor q llega es menor de 1000
WHEN (NEW.REGION_ID > 100)
BEGIN
    IF INSERTING THEN
        :NEW.REGION_NAME := UPPER(:NEW.REGION_NAME);
        INSERT INTO LOG_TABLE VALUES ('INSERT ROW', USER);
    END IF;
    IF UPDATING('REGION_NAME') THEN
        :NEW.REGION_NAME := UPPER(:NEW.REGION_NAME);
        INSERT INTO LOG_TABLE VALUES ('UPDATE REGION_NAME ROW', USER);
    END IF;
    IF DELETING THEN
        INSERT INTO LOG_TABLE VALUES ('DELETE ROW', USER);
    END IF;
    
    /*IF USER <> 'HR' THEN 
        RAISE_APPLICATION_ERROR(-20340, 'Solo HR puede insertar en la tabla regions');
    END IF;*/
END;
/

-- Probando trigger
INSERT INTO "HR"."REGIONS" (REGION_ID, REGION_NAME) VALUES (222, 'NICARAGUA');
--UPDATE HR.REGIONS SET REGION_NAME = LOWER(REGION_NAME);
--DELETE FROM HR.REGIONS WHERE REGION_ID = 110;

