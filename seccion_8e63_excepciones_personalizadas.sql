/*
Excepciones de usuario: Son aquellas excepciones que son creadas y controladas por el usuario
Se usa el comano 'RAISE' para lanzar una excepci�n personalizada
*/

SET SERVEROUTPUT ON
DECLARE
    REG_MAX EXCEPTION;
    NUMERO_REGION NUMBER;
    NOMBRE_REGION VARCHAR2(100);
BEGIN
    NUMERO_REGION := 99;
    NOMBRE_REGION := 'Pandora';
    
    IF NUMERO_REGION > 100 THEN
        RAISE REG_MAX;
    ELSE
        INSERT INTO hr.regions (region_id, region_name) VALUES (NUMERO_REGION, NOMBRE_REGION);
        COMMIT;
    END IF;
EXCEPTION
    WHEN REG_MAX THEN
        DBMS_OUTPUT.PUT_LINE('La regi�n no puede ser mayor de 100!');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error indefinido');

END;
