/*
Practica1: Crear un bloque que inserte un nuevo departamento en la tabla DEPARTMENTS
*/
/*
DECLARE
    LAST_DEPARTMENT_ID hr.departments.department_id%TYPE;
BEGIN 
    SELECT MAX(department_id) AS id_ultimo INTO LAST_DEPARTMENT_ID 
    FROM hr.departments;
    
    LAST_DEPARTMENT_ID := LAST_DEPARTMENT_ID + 1;
    INSERT INTO hr.departments (department_id, department_name, manager_id, location_id ) VALUES (LAST_DEPARTMENT_ID, 'INFORMÁTICA', 100, 1000);
    COMMIT;    
END;
*/

/*
Practica2: Crear un bloque PL/SQL que modifique la LOCATION_ID del nuevo departamento a 1700.
*/
/*
DECLARE
    
BEGIN
    UPDATE hr.departments SET location_id = 1700 WHERE department_id = 271;
    COMMIT;
END;
*/
/*
Practica 3: Por último hacer otro bloque PL/SQL que elimine ese departamento nuevo.
*/
DECLARE
    
BEGIN
    DELETE FROM hr.departments WHERE department_id = 271;
    COMMIT;
END;