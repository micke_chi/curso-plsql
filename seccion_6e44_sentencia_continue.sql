/*
La sentencia "CONTINUE" finaliza la iteración del LOOP y lo posiciona a la siguiente iteración
*/
DECLARE
    VAR1 NUMBER := 0;

BEGIN
    LOOP -- Con CONTINUE saltamos a la siguiente iteración;
        DBMS_OUTPUT.PUT_LINE('LOOP: VAR1 = ' || TO_CHAR(VAR1));
        VAR1 := VAR1 + 1;
        
        IF VAR1 < 3 THEN
            CONTINUE;
        END IF;
        
        DBMS_OUTPUT.PUT_LINE('Despues de continue: VAR1 = ' || TO_CHAR(VAR1));
        EXIT WHEN VAR1 = 5;
        
    END LOOP;

    DBMS_OUTPUT.PUT_LINE('DESPUES DEL LOOP: VAR1 = ' || TO_CHAR(VAR1));
END;