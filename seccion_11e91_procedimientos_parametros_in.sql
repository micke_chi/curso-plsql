/*
Par�metros dentro de PL/SQL PROCEDURES:
    - IN: Todos los parametros, si no tienen un tipo ser�n por defecto IN, son los par�metros de entrada, el valor no puede ser 
    modificado dentro del procedure
    - OUT: Par�metros de salida o retorno de los Procedures, este valor se guarda en la funcion que invoca al procedure
    - IN/OUT: Son parametros de entrada que tambi�n funcionan como par�metros de salida.
*/

/*
Par�metros de tipo IN
- Cada vez que hacemos modificaciones al procedure guardado tenemos que compilarlo para asegurar su funcionamiento
*/
-- Las variables de entrada son de SOLO LECTURA, por lo que no pueden ser modificadas
CREATE OR REPLACE PROCEDURE CALCULAR_IMPUESTO(
    EMPL_ID IN employees.employee_id%TYPE,
    IMPUESTO IN NUMBER
)

IS
    TAX NUMBER := 0;
    SALARIO NUMBER := 0;
BEGIN
    IF IMPUESTO < 0 OR IMPUESTO> 60 THEN 
        RAISE_APPLICATION_ERROR(-20004, 'El porcentaje debe estar entre 0 y 60');
    END IF;
    SELECT salary INTO SALARIO FROM employees WHERE employee_id = EMPL_ID;
    TAX := (SALARIO * IMPUESTO) / 100;
    DBMS_OUTPUT.PUT_LINE('Salario: ' || SALARIO);
    DBMS_OUTPUT.PUT_LINE('Impuesto: ' || TAX);
    
EXCEPTION
WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('No existe el empleado!');
END;
