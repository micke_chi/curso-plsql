/*
La sentencia "GOTO" hace que el codigo se bifurque o se redirija a la "block label" que indica
Para usar GOTO necesitamos tambien declara un BLOCK LABEL, al cual redirigira la sentencia GOTO
*/
DECLARE
  p  VARCHAR2(30);
  n  PLS_INTEGER :=4;
BEGIN
  /* Para saber si es numero primo sacamos su raiz cuadrada (SQRT), lo redondeamos y despues iteramos de manera ascendente. 
     por cadaiteraci�n obtenemos el residuo dela division de n / j, si el residuo = 0 , esto quiere decir que el numero es 
     divisible entre un numero que no es si mismo, por lo tanto no es un numero primo */
  FOR j in 2..ROUND(SQRT(n)) LOOP
    -- Compara el residuo de la divici�n y en base a esto decide si es primo o no
    IF n MOD j = 0 THEN
      p := ' no es un n�mero primo';
      -- Si no es primo redirigimos a otra linea mediante la instrucci�n GOTO que manda a imprimir un mensaje
      GOTO print_now;
    END IF;
  END LOOP;

  p := ' Es un n�mero primo';
 
  <<print_now>>
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(n) || p); 
END;
/