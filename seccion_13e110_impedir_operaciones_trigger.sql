-- Creando trigger que no permite la inserción de registros en la tabla regions a cualquier usuario 
CREATE OR REPLACE TRIGGER TR_REGION 
BEFORE INSERT ON REGIONS 
BEGIN
    IF USER <> 'HR' THEN 
        RAISE_APPLICATION_ERROR(-20340, 'Solo HR puede insertar en la tabla regions');
    END IF;
END;
/
-- Probando trigger
INSERT INTO "HR"."REGIONS" (REGION_ID, REGION_NAME) VALUES ('107', 'CHILE');