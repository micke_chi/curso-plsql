/*
Vistas para saber los elementos que tengo en la bd, funciones, procedures, etc.
*/

-- Vista USER_OBJECTS
    SELECT * FROM USER_OBJECTS WHERE OBJECT_TYPE = 'PROCEDURE';
-- Para ver todos los tipos de objetos que tenemos almacenado en la bd y su estado
    SELECT OBJECT_TYPE, COUNT(*) FROM USER_OBJECTS GROUP BY OBJECT_TYPE;
-- Para ver el codigo de un procedimiento
    SELECT * FROM USER_SOURCE WHERE NAME = 'PROCS1';