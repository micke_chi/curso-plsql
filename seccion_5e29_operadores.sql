SET SERVEROUTPUT ON
DECLARE
    /* OPERADORES MÁS HABITUALES
    +  SUMA 
    -  RESTA
    /  DIVISION
    *  MULTIPLICACION
    ** EXPONENTE
    || CONCATENAR
    */

    X NUMBER := 5;
    Z NUMBER := 10;
    A VARCHAR2(100) := 'EXAMPLE';
    D DATE := '2018-12-16';

BEGIN
    DBMS_OUTPUT.PUT('Suma: ');
    DBMS_OUTPUT.PUT_LINE(X + Z);
    
    DBMS_OUTPUT.PUT('Resta: ');
    DBMS_OUTPUT.PUT_LINE(X - Z);
    
    DBMS_OUTPUT.PUT('Multiplicación: ');
    DBMS_OUTPUT.PUT_LINE(X * Z);
    
    DBMS_OUTPUT.PUT('División: ');
    DBMS_OUTPUT.PUT_LINE(X / Z);
    
    DBMS_OUTPUT.PUT('Exponente: ');
    DBMS_OUTPUT.PUT_LINE(X ** 2);
    
    DBMS_OUTPUT.PUT_LINE('Concatenación de : ' || A);
    --Se pueden hacer operaciones con fechas
    DBMS_OUTPUT.PUT_LINE(D + 1);
    DBMS_OUTPUT.PUT_LINE(D);
    DBMS_OUTPUT.PUT_LINE(SYSDATE);
END;