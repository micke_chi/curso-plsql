/*
*Practica 1: Crear un procediemiento llamado visualizar que muestre le nombre y salario de todos los empleados
*/

/*
CREATE OR REPLACE PROCEDURE VISUALIZAR_NOMBRES_EMPLEADOS
IS
    CURSOR CURSOR1 IS SELECT first_name,salary FROM EMPLOYEES;
BEGIN 
    FOR i IN  CURSOR1 LOOP
        DBMS_OUTPUT.PUT_LINE('NOMBRE: ' || i.first_name || ', SALARIO: ' || i.salary);
    END LOOP;
END;

EXECUTE visualizar_nombres_empleados;

*/

/*
*Practica 2: Modificar el programa anterior para incluir un parametro que pase el numero de departamento para que
visualice solo los empleados de ese departamento
- Debera devolver el numero de empleados en una variable tipo out
*/

/*
CREATE OR REPLACE PROCEDURE VISUALIZA_EMPLEADOS_DEPTO(
    DEPTO_ID IN DEPARTMENTS.DEPARTMENT_ID%TYPE,
    CANTIDAD OUT NUMBER
)
IS
    CURSOR CURSOR1 IS SELECT first_name,salary FROM EMPLOYEES WHERE DEPARTMENT_ID = DEPTO_ID;
BEGIN 
    FOR i IN  CURSOR1 LOOP
        DBMS_OUTPUT.PUT_LINE('NOMBRE: ' || i.first_name || ', SALARIO: ' || i.salary);
        CANTIDAD := CURSOR1%ROWCOUNT;
    END LOOP;
    
END;
*/

/*Ejecuci�n del procedimiento*/

/*
SET SERVEROUTPUT ON
DECLARE
    DEPTO_ID DEPARTMENTS.DEPARTMENT_ID%TYPE;
    NUMERO_EMPLEADOS NUMBER;
BEGIN
    DEPTO_ID := 80;
    NUMERO_EMPLEADOS := 0;
    visualiza_empleados_depto(DEPTO_ID, NUMERO_EMPLEADOS);
    DBMS_OUTPUT.PUT_LINE('Empleados del departamento ' || DEPTO_ID || ': '|| NUMERO_EMPLEADOS);
END;
*/

/*
*Practica 3: crear un bloque por el cual se de formato a un n� de cuenta suministrado por completo, por ej , 11111111111111111111
- Formateado a: 1111-1111-11-1111111111
- Debemos usar un par�metro de tipo IN-OUT
*/

-- Opci�n 1
CREATE OR REPLACE PROCEDURE FORMATEAR_NUMERO(NUMERO_CUENTA IN OUT VARCHAR2)
IS
    CONTADOR NUMBER := 0;
    FORMATEADO VARCHAR2(100);
BEGIN
    FOR i IN 1..LENGTH(NUMERO_CUENTA) LOOP
        CONTADOR := CONTADOR + 1;
        FORMATEADO := FORMATEADO || SUBSTR( NUMERO_CUENTA, i, 1 );        
        IF(CONTADOR = 4 AND i < 9) THEN
            FORMATEADO := FORMATEADO || '-';
            CONTADOR := 0;
        ELSIF (CONTADOR = 2 AND i > 8 AND i < 11) THEN
            FORMATEADO := FORMATEADO || '-';
        END IF;
    END LOOP;
    NUMERO_CUENTA := FORMATEADO;
END;

-- Opci�n 2

CREATE OR REPLACE PROCEDURE FORMATEAR_NUMERO (numero IN OUT VARCHAR2)
IS
    guardar1 VARCHAR2(20);
    guardar2 VARCHAR2(20);
    guardar3 VARCHAR2(20);
    guardar4 VARCHAR2(20);
BEGIN
    guardar1:=substr(numero,1,4);
    guardar2:=substr(numero,5,4);
    guardar3:=substr(numero,9,2);
    guardar4:=substr(numero,10);
    numero:=guardar1 || '-' || guardar2 || '-' || guardar3 || '-' || guardar4;
END;
/

SET SERVEROUTPUT ON
DECLARE
    NUMERO_CUENTA VARCHAR2(100) := '11111111111111111111';
BEGIN
    formatear_numero(NUMERO_CUENTA);
    DBMS_OUTPUT.PUT_LINE('El n�mero de cuenta formateado: ' || NUMERO_CUENTA);
END;












