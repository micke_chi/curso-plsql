/*
Excepciones no predefinidas
*/

DECLARE
    MI_EXCEPCION EXCEPTION;
    -- PRAGMA, ordem al compilador pl/sql. Asocia uan excepci�n a un mensaje concew
    -- Todos los erroes oracle tienen un codigo numerico negativo, excepto la excepcion 100
    PRAGMA EXCEPTION_INIT(MI_EXCEPCION, -937);
    V1 NUMBER;
    V2 NUMBER;
BEGIN
    SELECT employee_id, SUM(salary) INTO V1, V2 FROM hr.employees;
    DBMS_OUTPUT.PUT_LINE(V1);
EXCEPTION
    WHEN MI_EXCEPCION THEN
        DBMS_OUTPUT.PUT_LINE('Funci�n de grupo incorrecta!');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error indefinido');

END;