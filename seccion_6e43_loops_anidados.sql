SET SERVEROUTPUT ON;
/* Loop normal
DECLARE
   X NUMBER:=1;
BEGIN
    LOOP 
     DBMS_OUTPUT.PUT_LINE(X);
     X:=X+1;
     EXIT WHEN X=11;
    END LOOP;
END;
*/
DECLARE 
    -- Es m�s rapido en operacion q integer ya que opera en la maquina binaria
    S PLS_INTEGER := 0;
    I PLS_INTEGER := 0;
    J PLS_INTEGER;
BEGIN
-- Estas etiquetas de bloque o 'Block label' sirven para obtener una referencia a las variables de este bloque con respecto a esta 
-- esto es util al trabajar con estrucuras anidadas y se desee tener acceso a las variables declaradas en este bloque o hacer referencia al bloque
    <<PARENT>>
    LOOP
        -- Print parent
        DBMS_OUTPUT.PUT_LINE('Parent I: ' || I);
        I := I + 1;
        J := 100;
        
        <<CHILD>>
        LOOP
            EXIT PARENT WHEN (I > 30);
            DBMS_OUTPUT.PUT_LINE('Child J: '|| J);
            J := J+1;
            EXIT CHILD WHEN (J > 105);            
        END LOOP CHILD;
        
    END LOOP PARENT;
END;