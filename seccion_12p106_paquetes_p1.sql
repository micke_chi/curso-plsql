/*
PRACTICA 1: Crear un paquete denominado REGIONES que tenga los siguientes componentes:
PROCEDIMIENTOS:
    -  ALTA_REGION, con par�metro de c�digo y nombre Regi�n. Debe devolver un error si la regi�n ya existe. Inserta una nueva 
    regi�n en la tabla. Debe llamar a la funci�n EXISTE_REGION para controlarlo.
    - BAJA_REGION, con par�metro de c�digo de regi�n y que debe borrar una regi�n. Debe generar un error si la regi�n no existe, 
    Debe llamar a la funci�n EXISTE_REGION para controlarlo
    -  MOD_REGION: se le pasa un c�digo y el nuevo nombre de la regi�n Debe modificar el nombre de una regi�n ya existente. Debe 
    generar un error si la regi�n no existe, Debe llamar a la funci�n EXISTE_REGION para controlarlo
FUNCIONES:
    CON_REGION. Se le pasa un c�digo de regi�n y devuelve el nombre 
    EXISTE_REGION. Devuelve verdadero si la regi�n existe. Se usa en los procedimientos y por tanto es PRIVADA, no debe
    aparecer en la especificaci�n del paquete
*/

CREATE OR REPLACE PACKAGE MIPACKAGE4
IS
    PROCEDURE ALTA_REGION(REGION_CODE NUMBER, REGION_NOMBRE VARCHAR2);
    PROCEDURE BAJA_REGION(REGION_CODE NUMBER);
    PROCEDURE MOD_REGION(REGION_CODE NUMBER, REGION_NOMBRE VARCHAR2);
    
    FUNCTION CON_REGION(REGION_CODE NUMBER) RETURN VARCHAR2;
END;
/
CREATE OR REPLACE PACKAGE BODY MIPACKAGE4
IS
    -- Funci�n CON_REGION
    FUNCTION CON_REGION(REGION_CODE NUMBER) RETURN VARCHAR2
    IS
        NOMBRE_REGION REGIONS.REGION_NAME%TYPE;
    BEGIN
        SELECT REGION_NAME INTO NOMBRE_REGION FROM REGIONS WHERE REGION_ID = REGION_CODE;
        RETURN NOMBRE_REGION;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            DBMS_OUTPUT.PUT_LINE('No existe una regi�n con el c�digo ' || REGION_CODE);
            RETURN '';
    END CON_REGION;
    
    -- Funci�n REGION_EXISTE    
    FUNCTION EXISTE_REGION(REGION_CODE NUMBER, REGION_NOMBRE VARCHAR2) RETURN BOOLEAN
    IS
        REGION_EXISTE REGIONS%ROWTYPE;
    BEGIN
        SELECT * INTO REGION_EXISTE FROM REGIONS WHERE REGION_ID = REGION_CODE OR REGION_NAME = REGION_NOMBRE;
        RETURN TRUE;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN FALSE;
    END EXISTE_REGION;
    
    -- Funci�n REGION_EXISTE    
    FUNCTION EXISTE_REGION(REGION_CODE NUMBER) RETURN BOOLEAN
    IS
        REGION_EXISTE REGIONS%ROWTYPE;
    BEGIN
        SELECT * INTO REGION_EXISTE FROM REGIONS WHERE REGION_ID = REGION_CODE;
        RETURN TRUE;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN FALSE;
    END EXISTE_REGION;
    
    -- Procedimiento ALTA_REGION
    PROCEDURE ALTA_REGION(REGION_CODE NUMBER, REGION_NOMBRE VARCHAR2)
    IS
        REGION_EXISTE BOOLEAN;
    BEGIN
        REGION_EXISTE := EXISTE_REGION(REGION_CODE, REGION_NOMBRE);
        IF(REGION_EXISTE) THEN
            DBMS_OUTPUT.PUT_LINE('NO SE AGREGO LA REGI�N');
            RAISE_APPLICATION_ERROR(-20012, 'Ya existe un registro con ese c�digo o nombre');
        ELSE
            DBMS_OUTPUT.PUT_LINE('REGION AGREGADA CORRECTAMENTE');
            INSERT INTO REGIONS (REGION_ID, REGION_NAME) VALUES(REGION_CODE, REGION_NOMBRE);
            COMMIT;
        END IF;
        
    END ALTA_REGION;
    
    -- Procedimiento BAJA_REGION
    PROCEDURE BAJA_REGION(REGION_CODE NUMBER)
    IS
        REGION_EXISTE BOOLEAN;
    BEGIN
        REGION_EXISTE := EXISTE_REGION(REGION_CODE);
        IF(REGION_EXISTE) THEN
            DBMS_OUTPUT.PUT_LINE('LA REGION SE ELIMINO CORRECTAMENTE');
            DELETE FROM REGIONS WHERE REGION_ID = REGION_CODE;
        ELSE
            DBMS_OUTPUT.PUT_LINE('LA REGION NO SE ELIMINO');
            RAISE_APPLICATION_ERROR(-20012, 'Error: No existe un registro con ese c�digo');
        END IF;
    END BAJA_REGION;
    
    PROCEDURE MOD_REGION(REGION_CODE NUMBER, REGION_NOMBRE VARCHAR2)
    IS 
        REGION_EXISTE BOOLEAN;
    BEGIN
        REGION_EXISTE := EXISTE_REGION(REGION_CODE);
        IF(REGION_EXISTE) THEN
            DBMS_OUTPUT.PUT_LINE('LA REGION SE MODIFICO');
            UPDATE REGIONS SET REGION_NAME = REGION_NOMBRE WHERE REGION_ID = REGION_CODE;
        ELSE
            DBMS_OUTPUT.PUT_LINE('LA REGION NO SE MODIFICO');
            RAISE_APPLICATION_ERROR(-20012, 'Error: No existe un registro con ese c�digo');
        END IF;
        
    END MOD_REGION;
    

END MIPACKAGE4;
/

SET SERVEROUTPUT ON
DECLARE
    NOMBRE_REGION VARCHAR2(100);
    REGION_CODE NUMBER;
BEGIN
    --mipackage4.alta_region(104, 'CUBA');
    --mipackage4.baja_region(104);
    mipackage4.mod_region(103, 'CUBA');
    --REGION_CODE := 105;
    --NOMBRE_REGION := mipackage4.con_region(REGION_CODE);
    --DBMS_OUTPUT.PUT_LINE('EL NOMBRE DE LA REGION ' || REGION_CODE || NOMBRE_REGION);

END;
/