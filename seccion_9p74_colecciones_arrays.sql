/*
Practicas de Colecciones y Records
*/
-- SELECT COUNT(*) FROM employees WHERE salary < 7000;
SET SERVEROUTPUT ON

DECLARE
    TYPE EMPLEADO IS RECORD (
        NAME VARCHAR2(100),
        SALARY employees.salary%TYPE,
        COD_DEPT employees.department_id%TYPE
    ); 
    
    TYPE EMPLEADOS IS TABLE OF
        EMPLEADO
    INDEX BY PLS_INTEGER;
    
    NOMBRE VARCHAR2(100);
    APELLIDO VARCHAR2(100);
    EMPLS EMPLEADOS;
BEGIN
    FOR i IN 100..206 LOOP
        SELECT first_name || '' || last_name AS nombre_completo, salary, department_id
        INTO EMPLS(i).NAME, EMPLS(i).SALARY, EMPLS(i).COD_DEPT  
        FROM employees
        WHERE employee_id = i;
    END LOOP;
    -- Visualizar toda la colecci�n
    FOR i IN EMPLS.FIRST..EMPLS.LAST LOOP
        DBMS_OUTPUT.PUT_LINE(EMPLS(i).NAME);
    END LOOP;
    -- Visualizar el primer empleado
        DBMS_OUTPUT.PUT_LINE('first: ' || EMPLS(EMPLS.FIRST).NAME);
    -- Visualizar el �ltimo empleado
        DBMS_OUTPUT.PUT_LINE('last: ' || EMPLS(EMPLS.LAST).NAME);
    -- Visualizar el numero de empleados
        DBMS_OUTPUT.PUT_LINE(EMPLS.COUNT);
    -- Borramos los empleados que ganen menos de 7000 y visualizamos de nuevo la coleccion
    FOR i IN EMPLS.FIRST..EMPLS.LAST LOOP
        IF (EMPLS(i).SALARY < 7000) THEN
            DBMS_OUTPUT.PUT_LINE('Eliminando: ' || EMPLS(i).NAME);
            EMPLS.DELETE(i);
        END IF;
    END LOOP;    
    -- Visualizamos el numero de empleados para ver cuantos se borraron
    DBMS_OUTPUT.PUT_LINE('Empleados despues de eliminar: ' || EMPLS.COUNT);
END;