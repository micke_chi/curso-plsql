/*
Creando el cuerpo de un package
*/
CREATE OR REPLACE PACKAGE MIPACK2
IS 
    PROCEDURE CONVERT_UPLO( NAME VARCHAR2, CONVERSION_TYPE CHAR);
END;
/

CREATE OR REPLACE PACKAGE BODY MIPACK2
IS
    -- Funciones privadas, solo accesibles dentro del paquete
    FUNCTION TO_UPPER(NAME VARCHAR)
    RETURN VARCHAR2
    IS
    BEGIN
        RETURN UPPER(NAME);
    END TO_UPPER;
    -- Funciones privadas, solo accesible dentro del paquete
    FUNCTION TO_LOWER(NAME VARCHAR2)
    RETURN VARCHAR2
    IS
    BEGIN
        RETURN LOWER(NAME);
    END TO_LOWER;
    
    -- Procedimiento publico ya que fue declara en la cabecera o especificaciones
    PROCEDURE CONVERT_UPLO(NAME VARCHAR2, CONVERSION_TYPE CHAR)
    IS
    BEGIN
        IF (CONVERSION_TYPE = 'U') THEN
            DBMS_OUTPUT.PUT_LINE(TO_UPPER(NAME));
        ELSIF (CONVERSION_TYPE = 'L') THEN
            DBMS_OUTPUT.PUT_LINE(TO_LOWER(NAME));
        ELSE
            DBMS_OUTPUT.PUT_LINE('El tipo de conversión debe ser U o L');
        END IF;
    END CONVERT_UPLO;
END MIPACK2;

/

SET SERVEROUTPUT ON
DECLARE
BEGIN
    mipack2.convert_uplo('mIGUEL aNGEl chI pECh', 'U');
END;
