/*
Practica1: Crear una SELECT (no un cursor expl�cito) que devuelva el nombre de un empleado pas�ndole el EMPLOYEE_ID en el WHERE
Practica2: Modificar la SELECT para que devuelva m�s de un empleado, por ejemplo poniendo EMPLOYEE_ID> 100
Practica3: Modificar la consulta para que devuelva un error de divisi�n por CERO
*/

/*
SET SERVEROUTPUT ON

DECLARE
    NOMBRE hr.employees.first_name%TYPE;
    CALCULO hr.employees.salary%TYPE;
BEGIN
    --SELECT first_name INTO NOMBRE FROM hr.employees WHERE employee_id = 23100;
    --SELECT first_name INTO NOMBRE FROM hr.employees;
    SELECT salary INTO CALCULO FROM hr.employees WHERE employee_id = 100;
    CALCULO := ( CALCULO / 0 );
    
    
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('ERROR, EMPLEADO INEXISTENTE');
    WHEN TOO_MANY_ROWS THEN
        DBMS_OUTPUT.PUT_LINE('ERROR, DEMASIADOS RESULTADOS, SE ESPERABA UNA SOLA FILA');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLCODE || '-' || SQLERRM);
END;
*/

/*
El error -00001 es clave primaria duplicada. Aunque ya existe una predefinida (DUP_VAL_ON_INDEX) vamos a crear una excepci�n no -predefinida que haga lo mismo
*/

SET SERVEROUTPUT ON

DECLARE

    DUPLICADO EXCEPTION;
    PRAGMA EXCEPTION_INIT(DUPLICADO, -00001);

BEGIN
    INSERT INTO hr.locations (location_id, postal_code, city, country_id) VALUES (1110, '97000', 'M�rida', 'MX');
    COMMIT;
EXCEPTION
    WHEN DUPLICADO THEN
        DBMS_OUTPUT.PUT_LINE('Error, la clave esta duplicada');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error, ha ocurrido un error desconocido');

END;

















