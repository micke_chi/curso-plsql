SET SERVEROUTPUT ON

DECLARE
    A VARCHAR2(50);
    MAYUS VARCHAR2(50);
    FECHA DATE;
    PRECIO NUMBER := 120.345;

BEGIN
    A := 'Ejemplo';
    -- Substrae parte de la cadena mediante los delimitadores que se le pasan como parametro;
    DBMS_OUTPUT.PUT_LINE(SUBSTR(A, 1, 3));
    -- Convierte todos los caracteres de la cadena a mayusculas
    MAYUS := UPPER(A);
    DBMS_OUTPUT.PUT_LINE(MAYUS);
    -- Obtiene la fecha actual
    FECHA := SYSDATE;
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(FECHA, 'day', 'NLS_DATE_LANGUAGE = spanish'));
    -- Redondea un numero a decimal o flotante
    DBMS_OUTPUT.PUT_LINE(FLOOR(PRECIO));
END;