/*
Los parámetros tipo OUT se usa para devolver un valor desde dentro del procedimiento.
- Cuando se pasa un paramtro de tipo OUT al procedimiento no se tiene el valor, simplemente es la posicion en
memoria donde se puede guardar un nuevo valor.
- Si cambio el valor de la variable dentro del procedimiento, el nuevo valor estara disponible para quien invoca al procedimiento
*/

create or replace PROCEDURE CALCULAR_IMPUESTO_OUT (
    EMPLEADO_ID IN EMPLOYEES.EMPLOYEE_ID%TYPE,
    T1 IN NUMBER, 
    R1 OUT NUMBER)
IS
    PSALARY NUMBER :=0;
    
BEGIN
    IF T1 < 0 OR T1 > 60 THEN
        RAISE_APPLICATION_ERROR(-2000, 'El porcentaje debe estar entr 0 y 60');
    END IF;
    
    SELECT SALARY INTO PSALARY FROM EMPLOYEES WHERE EMPLOYEE_ID = EMPLEADO_ID;
    R1 := PSALARY*T1/100;
    DBMS_OUTPUT.PUT_LINE('SALARY ' || PSALARY);
    
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('No existe el empleado');
END;