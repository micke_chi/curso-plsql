/**
*Practica 1: Crear un TRIGGER BEFORE DELETE sobre la tabla EMPLOYEES que impida borrar un registro si su JOB_ID es algo 
relacionado con CLERK
*/
/*
CREATE OR REPLACE TRIGGER TR_EMPLE1
BEFORE DELETE ON EMPLOYEES
FOR EACH ROW 
WHEN (OLD.JOB_ID LIKE '%CLERK%')
BEGIN
    RAISE_APPLICATION_ERROR(-20123, 'No puede eliminar este registro');
END;
/

SELECT * FROM EMPLOYEES WHERE JOB_ID LIKE '%CLERK%';
-- UPDATE EMPLOYEES SET EMAIL = UPPER(EMAIL) WHERE EMPLOYEE_ID = 125;
DELETE FROM EMPLOYEES WHERE EMPLOYEE_ID = 179;
TRUNCATE TABLE LOG_TABLE;

*/

/*
*Practica 2: Crear una tabla denominada AUDITORIA con las siguientes columnas
CREATE TABLE AUDITORIA (
USUARIO VARCHAR(50),
FECHA DATE,
SALARIO_ANTIGUO NUMBER,
SALARIO_NUEVO NUMBER);
*/

/*
* Practica 3: Crear un TRIGGER BEFORE INSERT de tipo STATEMENT, de forma que cada vez que se haga un INSERT en la 
* tabla REGIONS guarde una fila en la tabla AUDITORIA con el usuario y la fecha en la que se ha hecho el INSERT
*/
/*
CREATE OR REPLACE TRIGGER TR_REGIONS8
BEFORE INSERT ON REGIONS
BEGIN
    --SELECT TO_CHAR(SYSDATE, 'DD-MM-YYYY HH:MI:SS') INTO FECHA FROM DUAL;
    INSERT INTO AUDITORIA(USUARIO, FECHA) 
    VALUES(USER, SYSDATE);
END;
/
*/
-- Probando trigger
-- INSERT INTO REGIONS(REGION_ID, REGION_NAME) VALUES(18, 'HONDURAS');
-- UPDATE HR.REGIONS SET REGION_NAME = LOWER(REGION_NAME);
-- DELETE FROM HR.REGIONS WHERE REGION_ID = 110;

/*
*Practica 4: Realizar otro trigger BEFORE UPDATE de la columna SALARY de tipo EACH ROW. Si la modificaci�n supone rebajar
*el salario el TRIGGER debe disparar un RAISE_APPLICATION_FAILURE �no se puede bajar un salario�. Si el salario es mayor 
*debemos dejar el salario antiguo y el salario nuevo en la tabla AUDITORIA.
*/

/*
CREATE OR REPLACE TRIGGER TR_EMPLE2
BEFORE UPDATE OF SALARY ON EMPLOYEES
FOR EACH ROW
BEGIN
    IF(:NEW.SALARY < :OLD.SALARY) THEN
        RAISE_APPLICATION_ERROR(-20456, 'No se puede bajar un salario');
    ELSE
        INSERT INTO AUDITORIA(USUARIO, FECHA, SALARIO_ANTIGUO, SALARIO_NUEVO)
        VALUES(USER, SYSDATE, :OLD.SALARY, :NEW.SALARY);
    END IF;
END;
/
-- Probamos el trigger
UPDATE EMPLOYEES SET SALARY = 9000 WHERE EMPLOYEE_ID = 203;

*/

/*
*Practica 5: Crear un TRIGGER BEFORE INSERT en la tabla DEPARTMENTS que al insertar un departamento compruebe que el c�digo no
est� repetido y luego que si el LOCATION_ID es NULL le ponga 1700 y si el MANAGER_ID es NULL le ponga 200
*/

CREATE OR REPLACE TRIGGER TR_DEPTO1
BEFORE INSERT ON DEPARTMENTS
FOR EACH ROW
DECLARE
    DEPTO DEPARTMENTS%ROWTYPE;
    CURSOR C1(DEPTO_ID NUMBER) IS SELECT * FROM DEPARTMENTS WHERE DEPARTMENT_ID = DEPTO_ID;
BEGIN
    OPEN C1(:NEW.DEPARTMENT_ID);
        FETCH C1 INTO DEPTO;
            IF (C1%FOUND) THEN
                RAISE_APPLICATION_ERROR(-20000, 'Ya existe un registro con el mismo ID');
            ELSE
                IF(:NEW.LOCATION_ID IS NULL) THEN
                    :NEW.LOCATION_ID := 1700;
                END IF;
                IF(:NEW.MANAGER_ID IS NULL) THEN
                    :NEW.MANAGER_ID := 200;
                END IF;
            END IF;
    CLOSE C1;       
END;
/

-- Probando trigger
INSERT INTO DEPARTMENTS(DEPARTMENT_ID, DEPARTMENT_NAME)
VALUES (280, 'Recreation');

SELECT * FROM DEPARTMENTS WHERE DEPARTMENT_ID = 8450;



