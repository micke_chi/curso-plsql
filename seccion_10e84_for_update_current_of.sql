/*
Updates y Deletes usando las clausulas FOR UPDATE y CURRENT OF en un cursor
*/

DECLARE

    CURSOR CURSOR1 IS SELECT * FROM employees FOR UPDATE;
    EMPLEADO employees%ROWTYPE;

BEGIN

    OPEN CURSOR1;
        LOOP
            FETCH CURSOR1 INTO EMPLEADO;
            EXIT WHEN CURSOR1%NOTFOUND;
            IF EMPLEADO.commission_pct IS NOT NULL THEN
                -- Actualizamos el registro si la comision en nula, CURRENT OF actualiza el registro que corresponda con la fila leida actualmente
                UPDATE employees SET salary = salary*1.10 WHERE CURRENT OF CURSOR1;
            ELSE
                UPDATE employees SET salary = salary*1.15 WHERE CURRENT OF CURSOR1;
            END IF;  
        END LOOP;
        
    CLOSE CURSOR1;

END;