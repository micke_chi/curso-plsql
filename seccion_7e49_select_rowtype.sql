/*
Uso de querys basicas que devuelven un solo registro como resultado
dentro de pl/sql, usamos 'INTO' para asignar el resultado de la query en una varible del bloque pl/sql
*/
/*
SET SERVEROUTPUT ON;

DECLARE

    SALARIO NUMBER;
    -- Declaramos una variable con 
    NOMBRE hr.employees.first_name%TYPE;
    
BEGIN
    -- Usamos la sentencia 'INTO' para  guardar el resultado de la consulta en la variable SALARIO que declaramos
    -- Esta query solo debe devolver una respuesta, ya que se estan esperando resultados unicos
    SELECT salary, first_name INTO SALARIO, NOMBRE FROM hr.employees WHERE employee_id = 100;
    
    DBMS_OUTPUT.PUT_LINE( NOMBRE || ' : ' || SALARIO );

END;

*/

/*
Uso %ROWTYPE para que el resultado sea asignado como una fila con todos los registros y no columna por columna como %TYPE
*/
SET SERVEROUTPUT ON;

DECLARE

    -- SALARIO NUMBER;
    -- Declaramos una variable con 
    -- NOMBRE hr.employees.first_name%TYPE;
    -- Declaramos una variable que contendra el resultado completo de una fina de una tabla y ponerla en una sola varibale para 
    -- que posteriormente pueda acceder a cualquier columna del registro
    EMPLEADO hr.employees%ROWTYPE;
    
BEGIN
    -- Usamos la sentencia 'INTO' para  guardar el resultado de la consulta en la variable SALARIO que declaramos
    -- Esta query solo debe devolver una respuesta, ya que se estan esperando resultados unicos
    SELECT * INTO EMPLEADO FROM hr.employees WHERE employee_id = 100;
    
    DBMS_OUTPUT.PUT_LINE( EMPLEADO.first_name || ' : ' || EMPLEADO.salary );

END;