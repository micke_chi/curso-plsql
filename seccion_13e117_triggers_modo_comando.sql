/*
*Trabajar con triggers en modo comando.
*/
-- Compila un trigger
ALTER TRIGGER TR_REGION6 COMPILE;

-- Para ver los ultimos errores
SELECT * FROM USER_ERRORS;

-- Para desactivar un trigger
ALTER TRIGGER TR_REGION6 ENABLE;