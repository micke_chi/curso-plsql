/*
CREACI�N DE CURSORES
    - CURSORES IMPLICITOS: Son aquellos que se generan automaticamente cuando lanzo un comando. Oracle crea automaticamente cursores 
    cuando hacemos por ejemplo un query SELECT
    - CURSORES EXPLICITOS: Aquel en que yo defino la select, lo habro, lleno, me muevo a trav�s de el y cierro 
    - M�todos �tiles en cursores: Estos m�todos se usan de manera inmediata al cursor. el prefijo 'SQL' hace referencia a que es un
    cursor implicito, si fuese un cursor explicito ese prefijo seria 'nombre_del_cursor'.
    -- SQL%ISOPEN: Booleano que indica si el cursor esta abierto o cerrado, no aplica para cursores impl�citos
    -- SQL%FOUND: Booleano que indica si se encontraron registros
    -- SQL%NOTFOUND Booleano que indica que no se encontro ningun registro
    -- SQL%ROWCOUNT Retorna el numero de registros procesados

*/
SET SERVEROUTPUT ON

DECLARE
    X NUMBER;
    
BEGIN
    UPDATE TEST SET C2 = 'Prueba cursor' WHERE C1 = 122;
    DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT);
    -- En Insert, update, delete, es posible usar FOUND o NOTFOUND para saber si el registro existe o no existe y saber como proceder, esto no 
    -- es igual en los SELECTS ya que si no hay filas me devuelve una excepci�n, por lo cual el programa se rompe.
    IF(SQL%FOUND) THEN
        DBMS_OUTPUT.PUT_LINE('Encontrado');
    ELSIF (SQL%NOTFOUND) THEN
        DBMS_OUTPUT.PUT_LINE('No encontrado');
    END IF;
    
    -- Al hacer una QUERY de tipo SELECT como la siguiente, si no existen resultados para la consulta devuelve una excepcion y rompe el programa
    --SELECT C1 INTO X FROM TEST WHERE C1 = 1000;

END;
