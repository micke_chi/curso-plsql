/*
Trabajando con inserts y updates con plsql Records.
Podemos hacer inserciones y actualizaciones de registros de manera m�s eficiente con Records
*/

-- CREATE TABLE REGIONES AS SELECT * FROM regions WHERE REGION_ID = 0;
DECLARE
    REGION1 regions%ROWTYPE;
    
BEGIN
    SELECT * INTO REGION1 FROM regions WHERE region_id = 1;
    -- Rellenamos todos los valores del registro a insertar con el objeto RECORD REGION1
    INSERT INTO regiones VALUES REGION1;
    COMMIT;   
    
END;
/

DECLARE 
    REGION1 regions%ROWTYPE;
    
BEGIN
    REGION1.region_id := 1;
    REGION1.region_name := 'M�xico';
    -- Al actualiar usamos 'ROW' para indicar que rellenaremos los valores en el update con los valores del RECORD REGION1
    UPDATE regiones SET ROW = REGION1 WHERE region_id = 1;

END;