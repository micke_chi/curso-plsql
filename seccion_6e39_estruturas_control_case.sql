DECLARE
    V1 CHAR(1);
    V2 NUMBER;
    V3 V2%TYPE;
BEGIN
    V1 := 'y';
    V2 := 10;
    V3 := 72;
    
    CASE V1
        WHEN 'A' THEN DBMS_OUTPUT.PUT_LINE('Excelente');
        WHEN 'B' THEN DBMS_OUTPUT.PUT_LINE('Muy bien');
        WHEN 'C' THEN DBMS_OUTPUT.PUT_LINE('Bien');
        WHEN 'D' THEN DBMS_OUTPUT.PUT_LINE('Suficiente');
        ELSE DBMS_OUTPUT.PUT_LINE('Reprobado');
    END CASE;
    
    CASE V2
        WHEN 10 THEN DBMS_OUTPUT.PUT_LINE('Excelente2');
        WHEN 20 THEN DBMS_OUTPUT.PUT_LINE('Muy bien2');
        WHEN 30 THEN DBMS_OUTPUT.PUT_LINE('Bien2');
        WHEN 40 THEN DBMS_OUTPUT.PUT_LINE('Suficiente2');
        ELSE DBMS_OUTPUT.PUT_LINE('Reprobado2');
    END CASE;
    
    CASE
        WHEN V3 >= 70 THEN DBMS_OUTPUT.PUT_LINE('Aprobado');
        WHEN V3 >= 50 AND V3 < 70 THEN DBMS_OUTPUT.PUT_LINE('Tienen 2da oportunidad');
        ELSE DBMS_OUTPUT.PUT_LINE('Reprobado3');
    END CASE;

END;