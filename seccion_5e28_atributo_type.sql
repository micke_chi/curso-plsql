-- ATRIBUTO %TYPE EN DECLARACION DE VARIABLES
-- Se utiliza el atributo %TYPE para asegurar la compatibilidad entre datos

DECLARE
    X NUMBER;
    Z X%TYPE;
    EMPLEADO EMPLOYEES.SALARY%TYPE;
BEGIN
    EMPLEADO := 100;
END;

