/*
SQLCODE: Devuelve el codigo de error que retorna la excepcion
SQLERRM: Devuelve el mensaje del error que retorna la excepcion
*/

SET SERVEROUTPUT ON
DECLARE
   EMPL hr.employees%ROWTYPE;
   CODE NUMBER;
   MESSAGE VARCHAR2(100);
BEGIN
   SELECT * INTO EMPL FROM hr.employees;
    DBMS_OUTPUT.PUT_LINE(EMPL.salary);
EXCEPTION   
   WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLCODE);
        DBMS_OUTPUT.PUT_LINE(SQLERRM);
        CODE:=SQLCODE;
        MESSAGE:=SQLERRM;
        INSERT INTO ERRORS VALUES (CODE,MESSAGE);
        COMMIT;
END;
