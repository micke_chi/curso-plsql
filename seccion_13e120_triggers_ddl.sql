/*
*TRIGGERS DE TIPO DDL: Son los triggers son lanzados en eventos de operacion sobre esquemas
*/

CREATE OR REPLACE TRIGGER TR_DDL_DEMO1 
BEFORE DROP ON HR.SCHEMA 
BEGIN
  RAISE_APPLICATION_ERROR(-20100, 'No se pueden borrar las tablas');
END;
/
DROP TABLE LOG_TABLE;