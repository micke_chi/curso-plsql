/*
CREANDO CURSORES Y RECORRIENDO CON BUCLE LOOP: 
*/

DECLARE
    -- 1.- Crear cursor
    -- Mientras no lo abramos aun no reserva espacio en memoria para guardar los resultados
    CURSOR CURSOR1 IS SELECT * FROM regions;
    -- Declaramos la variable en donde guardaremos los resultados del cursor
    V1 regions%ROWTYPE;

BEGIN
    -- Abrimos el cursor
    OPEN CURSOR1;
        -- Recorriendo resultados con LOOP
        LOOP
            -- Leemos el cursor con el comando FETCH
            FETCH CURSOR1 INTO V1;
            EXIT WHEN CURSOR1%NOTFOUND;
            DBMS_OUTPUT.PUT_LINE(V1.region_name);
        END LOOP;        
    -- Cerramos el cursor
    CLOSE CURSOR1;
    
    -----------------------------------------------------
    DBMS_OUTPUT.PUT_LINE('------------con for------------');
    -- Recorriendo resultados con FOR
    -- Cuando se usa un bucle for el Abrir, Cerrar, Leer con el Fetch se hace de manera implicita, as� que solo procesamos 
    -- los datos del cursor
    FOR i IN CURSOR1 LOOP
        -- Leemos el cursor con el comando
        DBMS_OUTPUT.PUT_LINE(i.region_name);
    END LOOP;
    
END;