/*
El bucle FOR permite entre sus capacidades, crear un cursor de manera implicita en su declaracion, esto podiendo la query directamente en el for;
*/
/*
SET SERVEROUTPUT ON

BEGIN
    FOR i IN (SELECT * FROM regions) LOOP
        DBMS_OUTPUT.PUT_LINE(i.region_name);
    END LOOP;
END;
*/
/*
CURSORES CON PARÁMETROS: es posible pasarle parámetros al cursor para usarlos en la consulta;
*/
SET SERVEROUTPUT ON
DECLARE 
    CURSOR CURSOR1(PSAL NUMBER) IS SELECT * FROM employees WHERE salary < PSAL;
    EMPLEADO employees%ROWTYPE;
BEGIN
    OPEN CURSOR1(3000);
        LOOP
            FETCH CURSOR1 INTO EMPLEADO;
            EXIT WHEN CURSOR1%NOTFOUND;
            DBMS_OUTPUT.PUT_LINE(EMPLEADO.first_name || ' - ' || EMPLEADO.salary);
        END LOOP;
        DBMS_OUTPUT.PUT_LINE('Se han encontrado ' || CURSOR1%ROWCOUNT || ' registros');
    CLOSE CURSOR1;
END;