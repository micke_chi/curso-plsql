/*
* SOBRECARGA DE PROCEDIMIENTOS O FUNCIONES: Tal cual como en java, se trata de tener funciones o procedimientos con el mismo nombre
pero argumentos diferentes, de ahi que el comportamiento sera diferente dependiendo de los argumentos que se le pasen al metodo.
 - polimorfimo: relacionado con la herencia
 - sobrecarga: no esta directamente relacionado con la herencia como el polimorfismo

*/

CREATE OR REPLACE PACKAGE MIPACK3
IS
    FUNCTION COUNT_EMPLOYEES(ID NUMBER) RETURN NUMBER;
    FUNCTION COUNT_EMPLOYEES(ID VARCHAR2) RETURN NUMBER;
END;
/

CREATE OR REPLACE PACKAGE BODY MIPACK3
IS
    FUNCTION COUNT_EMPLOYEES(ID NUMBER) RETURN NUMBER
    IS
        VARNUM NUMBER;
    BEGIN
        SELECT COUNT(*) INTO VARNUM 
        FROM EMPLOYEES 
        WHERE DEPARTMENT_ID = ID;
        RETURN VARNUM;
    END;
    
    FUNCTION COUNT_EMPLOYEES(ID VARCHAR2) RETURN NUMBER
    IS
        VARNUM NUMBER;
    BEGIN
        SELECT COUNT(*) INTO VARNUM 
        FROM EMPLOYEES E, DEPARTMENTS D 
        WHERE DEPARTMENT_NAME = ID 
        AND E.DEPARTMENT_ID = D.DEPARTMENT_ID;
        RETURN VARNUM;
    END;

END MIPACK3;
/

SET SERVEROUTPUT ON
DECLARE
BEGIN
    DBMS_OUTPUT.PUT_LINE(mipack3.count_employees(50));
END;