/*
 Colecciones y array asociativos: Son como los arrays de la programaci�n normal 
 Trabajando con arrays asociativos, son de tipo clave - valor(escalar o record)
 M�todos para manejo de arrays:
    - EXIST(N): Detecta si existe un elemento de un array
    - COUNT: Retora el numero de elementos del array
    - FIRST: Retorna el primer elemento del array
    - LAST: Retorna el ultimo elemento del arryay
    - PRIOR(N):Retorna el �ndice anterior a N
    - NEXT(N): Devuelve el �ndice posterior a N
    - DELETE: Borra todo el array
    - DELETE(N): Borra el elemento del array indicado
    - DELETE (N, M): Borra un rango de elementos indicados en los indices M, N
*/

SET SERVEROUTPUT ON

DECLARE 
    /*Declaramos los tipo de arrays que usaremos*/
    -- Creamos una array simple que guarda nombre de departamentos
    TYPE DEPARTAMENTOS IS TABLE OF
        departments.department_name%TYPE
    INDEX BY PLS_INTEGER;
    -- Creamos un array asociatio compuesto que puede guardar multiples objetos de tipo Record (Rowtype, filas completas de tablas)
    TYPE EMPLEADOS IS TABLE OF
        employees%ROWTYPE
    INDEX BY PLS_INTEGER;
    
    -- Creamos un array de los tipos de arrays que declaramos anteriormente
    DEPTOS DEPARTAMENTOS;
    EMPLS EMPLEADOS;
    
BEGIN
    
    -- TIPO SIMPLE
    -- Insertamos valores a un array simple a trav�s de su indice Array(n)
    DEPTOS(1) := 'Inform�tica';
    DEPTOS(2) := 'Finanzas';
    -- Accesamos a los valores del array simple a traves de su indice
    DBMS_OUTPUT.PUT_LINE(DEPTOS(1));
    DBMS_OUTPUT.PUT_LINE(DEPTOS(2));
    -- Usando m�todos de manipulaci�n de arrays
    DBMS_OUTPUT.PUT_LINE(DEPTOS.LAST);
    IF DEPTOS.EXISTS(3) THEN
        DBMS_OUTPUT.PUT_LINE(DEPTOS(3));
    ELSE
        DBMS_OUTPUT.PUT_LINE('El departamento no existe');
    END IF;
    
    -- TIPO COMPLEJO
    -- Insertamos 2 registros de la tabla employees al array a trav�s de un �ndice Array(N)
    SELECT * INTO EMPLS(1) FROM employees WHERE employee_id = 100;
    SELECT * INTO EMPLS(2) FROM employees WHERE employee_id = 110;
    -- Accedemos a los valores de un array complejo(array de records) a trav�s de Array(n).campo
    DBMS_OUTPUT.PUT_LINE(EMPLS(1).first_name);
    DBMS_OUTPUT.PUT_LINE(EMPLS(2).first_name);
    
END;