/*
TRIGERS: Son disparadores que se ejecutan antes o despues de cierto evento
- Existenn 3 tipo de triggers:
    - DML: Permite gestionar insert, update y delet
    - DDl: Permite controlar operaciones sobre objetos, creates drops
    - Database: Permite controlar operaciones concretas de la BD, cuando se arranca la bd o se apaga etc.
- Tipos de trigger
    - Before: Son aquellos que se lanzan antes de una operación
    - After: Son aquellos que se lanzan despues de una operación
    - Instanced Of: Se usan en vistas
- Eventos que disparan triggers
    - Insert
    - Update
    - Delete
- Escenarios para lanzar triggers
    - Statement: Cuando necesito que el triger sea lanzado un vez por toda la operacion, no por las filas afectadas
    - Row: Cuando quiero que el trigger sea lanzado por cada fila afectada.
-    
*/

CREATE OR REPLACE TRIGGER INRT_EMPLEADO
AFTER INSERT ON REGIONS
BEGIN
    INSERT INTO LOG_TABLE VALUES('INSERCIÓN EN LA TABLA REGIONS', USER);
END;

-- Probando trigger
INSERT INTO "HR"."REGIONS" (REGION_ID, REGION_NAME) VALUES ('102', 'CANADA')

