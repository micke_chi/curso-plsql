/**
*TRIGGERS ROW: Se disparan por cada filas afectada por el trigger, para lo cual tienen los tipos before row, affter row
* a diferencia de los triggers de tipo statement, en estos triggers puedo controlar ademas de la operación, puedo controlar
* el tipo de dato en la operación a la fila.
* - Existen 2 tipos de datos
*   - :OLD: Me permite acceder a un valor antiguo en la fila previo a la operación lanzada en el trigger (update, delete)
*   - :NEW: Mermite acceder al valor nuevo despues de aplicar la operación (insert, update)
*/

-- Creando Trigger que controla multiples eventos
CREATE OR REPLACE TRIGGER TR_REGION5
BEFORE INSERT OR UPDATE OF REGION_NAME OR DELETE 
ON REGIONS
-- Con esto el indicamos que el trigger se debe ejecutar por cada fila
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        :NEW.REGION_NAME := UPPER(:NEW.REGION_NAME);
        INSERT INTO LOG_TABLE VALUES ('INSERT ROW', USER);
    END IF;
    IF UPDATING('REGION_NAME') THEN
        :NEW.REGION_NAME := UPPER(:NEW.REGION_NAME);
        INSERT INTO LOG_TABLE VALUES ('UPDATE REGION_NAME ROW', USER);
    END IF;
    IF DELETING THEN
        INSERT INTO LOG_TABLE VALUES ('DELETE ROW', USER);
    END IF;
    
    /*IF USER <> 'HR' THEN 
        RAISE_APPLICATION_ERROR(-20340, 'Solo HR puede insertar en la tabla regions');
    END IF;*/
END;
/

-- Probando trigger
--INSERT INTO "HR"."REGIONS" (REGION_ID, REGION_NAME) VALUES (110, 'BOLIVIA');
UPDATE HR.REGIONS SET REGION_NAME = LOWER(REGION_NAME);
--DELETE FROM HR.REGIONS WHERE REGION_ID = 110;