/*
Practica1: Hacer un programa que tenga un cursor que vaya visualizando los salarios de los empleados. Si en el cursor aparece 
el jefe (Steven King) se debe generar un RAISE_APPLICATION_ERROR indicando que el sueldo del jefe no se puede ver.
*/

/*
SET SERVEROUTPUT ON

DECLARE 
    CURSOR CURSOR1 IS SELECT * FROM employees;
BEGIN
    FOR i IN CURSOR1 LOOP
        IF i.employee_id = 100 THEN
            RAISE_APPLICATION_ERROR(-20002, 'Error, el sueldo del jefo no se puede ver!!');
        END IF;
        DBMS_OUTPUT.PUT_LINE(i.first_name || ' - ' || i.salary);
    END LOOP;
END;

*/

/*
Practica2: Hacemos un bloque con dos cursores. (Esto se puede hacer f�cilmente con una sola SELECT pero vamos a hacerlo de 
esta manera para probar par�metros en cursores)
*/
/*
SET SERVEROUTPUT ON

DECLARE
    CURSOR CURSOR1 IS SELECT * FROM employees;
    CURSOR CURSOR2(PMANAGER_ID departments.manager_id%TYPE) IS SELECT * FROM departments WHERE manager_id = PMANAGER_ID;    
    DEPTO departments%ROWTYPE;
BEGIN
    FOR i IN CURSOR1 LOOP
        OPEN CURSOR2(i.employee_id);
            FETCH CURSOR2 INTO DEPTO;
            IF CURSOR2%FOUND THEN 
                DBMS_OUTPUT.PUT_LINE(i.first_name || ' departamento: ' || DEPTO.department_name);
            ELSE
                DBMS_OUTPUT.PUT_LINE(i.first_name || ' No es jefe de nada');
            END IF;
        CLOSE CURSOR2;
        
    END LOOP;
END;
*/

/*
Practica3: Crear un cursor con par�metros que pasando el n�mero de departamento visualice el n�mero de empleados de ese departamento
*/

/*
SET SERVEROUTPUT ON

DECLARE
    CURSOR CURSOR1(PDEPTO_ID EMPLOYEES.department_id%TYPE) IS SELECT COUNT(*) FROM employees WHERE department_id = PDEPTO_ID;    
    NUMERO_EMPLEADOS NUMBER;
BEGIN
    OPEN CURSOR1(80);
        FETCH CURSOR1 INTO NUMERO_EMPLEADOS;
        IF CURSOR1%FOUND THEN 
            DBMS_OUTPUT.PUT_LINE('Empleados: ' || NUMERO_EMPLEADOS);
        ELSE
            DBMS_OUTPUT.PUT_LINE('No tiene empleados');
        END IF;
    CLOSE CURSOR1;
END;
*/


/*
Practica4: Crear un bucle FOR donde declaramos una subconsulta que nos devuelva el nombre de los empleados que sean ST_CLERCK. 
Es decir, no declaramos el cursor sino que lo indicamos directamente en el FOR.
*/
/*
SET SERVEROUTPUT ON

BEGIN
    FOR i IN (SELECT * FROM employees WHERE job_id = 'ST_CLERK') LOOP
        DBMS_OUTPUT.PUT_LINE(i.first_name);
    END LOOP;
END;
*/


/*
Practica5: Creamos un bloque que tenga un cursor para empleados. Debemos crearlo con FOR UPDATE.
*/

SET SERVEROUTPUT ON

DECLARE
    CURSOR CURSOR1 IS SELECT * FROM employees FOR UPDATE;
    CITEM employees%ROWTYPE;
BEGIN
    OPEN CURSOR1;
        LOOP
            FETCH CURSOR1 INTO CITEM;
            EXIT WHEN CURSOR1%NOTFOUND;
            IF CITEM.salary > 8000 THEN
                UPDATE employees SET salary = (salary * 1.02) WHERE CURRENT OF CURSOR1;
            ELSE
                UPDATE employees SET salary = (salary * 1.03) WHERE CURRENT OF CURSOR1;  
            END IF;
        END LOOP;
    CLOSE CURSOR1;
END;







