-- CONSTANTES Y NULL

DECLARE
    -- Una constante no puede ser sobrescrita una vez que su valor inicia ha sido asignado
    X CONSTANT NUMBER := 10; 
    -- Si mantengo esta variable sin asignar ningun valor me arrojara un error ya que declare como NOT NULL;
    -- Z NUMBER NOT NULL;
    Z NUMBER NOT NULL := 20;
BEGIN
    DBMS_OUTPUT.PUT_LINE(Z);
    
    -- Si intento cambiar el valor de x me arrojara un error ya que esta variable fu declarada como constante
    --X CONSTANT NUMBER := 30; 
    DBMS_OUTPUT.PUT_LINE(X);
END;