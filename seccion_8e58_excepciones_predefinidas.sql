/*
Manejo de excepciones
*/

DECLARE
    EMPLEADO hr.employees%ROWTYPE;
BEGIN
    -- SELECT * INTO EMPLEADO FROM hr.employees WHERE employee_id = 1000;
    SELECT * INTO EMPLEADO FROM hr.employees;
    DBMS_OUTPUT.PUT_LINE(EMPLEADO.first_name);
EXCEPTION
    -- Las excepciones m�s comunes en oracle
    -- NOD_DATA_FOUND: Cuando en una select no encuentra ningun resultado ORA-01403
    -- TOO_MANY_ROWS: Cuando la query devuelve m�s registros de los esperados
    -- ZERO_DIVIDE: Cuando se intenta dividir un valor entre 0
    -- DUP_VAL_ON_INDEX: Cuando la clave primaria ya existe
    
    --La sintaxis de un error es la siguiente
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('ERROR, EMPLEADO NO EXISTE');
    WHEN TOO_MANY_ROWS THEN
        DBMS_OUTPUT.PUT_LINE('ERROR, DEMASIADOS EMPLEADOS');
    WHEN OTHERS THEN 
    NULL;
    

END;