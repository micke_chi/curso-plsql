/*
* Uso del Package UTL_FILE para leer archivos, existen m�s paquetes de oracle que podemos usar para resolver alguna necesidad
- Para usar UTL_FILE hay que tener ciertos permisos de usuario:
    - GRANT CREATE ANY DIRECTORY TO HR;
    - GRANT EXECUTE ON SYS.UTL_FILE TO HR;
- Crear un directorio es un objeto de oracle que enlaza mapeo un directorio fisico del SO con un objeto directory de oracle
*/

-- Se crea un directorio
--CREATE OR REPLACE DIRECTORY EXCERCISES AS 'C:\oracle-exercises';

CREATE OR REPLACE PROCEDURE LEER_ARCHIVO
IS
    CADENA VARCHAR2(5000);
    -- varible de tipo utl_file
    VFILE UTL_FILE.FILE_TYPE;
BEGIN
    -- Abrimoms el archivo y lo asignamos a la variable de tipo archivo
    VFILE := UTL_FILE.FOPEN('EXERCISES', 'ejemplo.txt', 'R');
    LOOP
        BEGIN
            -- Leemos el archiv linea por linea y lo asignamos a la variable cadena
            UTL_FILE.GET_LINE(VFILE, CADENA);
            -- imprimimos las lineas a traaves de la variable cadena
            INSERT INTO LINEAS_ARCHIVO (LINEA) VALUES(CADENA);
            COMMIT;
            DBMS_OUTPUT.PUT_LINE(CADENA);
            EXCEPTION
            -- Cuando no haya m�s lineas se emitira una excepcion NO_DATA_FOUND la cual nos servira para cerrar el LOOP
            WHEN NO_DATA_FOUND THEN
                EXIT;
        END;
    END LOOP;
    UTL_FILE.FCLOSE(VFILE);
END;
/

-- Ejecutamos el procedimiento para leer un fichero
SET SERVEROUTPUT ON
DECLARE
BEGIN
    LEER_ARCHIVO;
END;
/

-- Creamos una secuencia para agregar la columna auto increment a la tabla, en las versiones de oracle < 12c, es necesario hacerlo
-- mediante un trigger
-- Creamos la sequence
CREATE SEQUENCE books_sequence;
-- Creamos una trigger para incrementar la sequence y agregarlo a la tabla
CREATE OR REPLACE TRIGGER LINEAS_ARCHIVO_ON_INSERT
    BEFORE INSERT ON LINEAS_ARCHIVO
    FOR EACH ROW
BEGIN
    SELECT LINEAS_ARCHIVO_SEQUENCE.NEXTVAL
    INTO :NEW.ID
    FROM DUAL;
END;