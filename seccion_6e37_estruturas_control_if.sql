/*Operaciones relacionales o de comparaci�n
    = igual a
    <> diferente, distinto de
    < menor
    > mayor
    <= menor que
    >= mayor que
    
    Operadores l�gicos
    
    AND (Y) y l�gico
    NOT negaci�n
    OR (o) o l�gico

*/
DECLARE
    VENTAS NUMBER := 10000; 
    BONOS NUMBER := 0; 
BEGIN

    IF VENTAS > 20000 THEN
        DBMS_OUTPUT.PUT_LINE('Las ventas son mayores a 120000: ' || VENTAS);
        BONOS := 2000;
    ELSIF VENTAS >= 10000 THEN
        DBMS_OUTPUT.PUT_LINE('Las ventas son mayores a 10000: ' || VENTAS);
        BONOS := 1000;
    ELSE
        DBMS_OUTPUT.PUT_LINE('No se cumplieron los objtivos de ventas');
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('Ventas: ' || VENTAS || ', BONOS: ' || BONOS);
    
END;