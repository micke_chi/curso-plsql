SET SERVEROUTPUT ON

DECLARE
    X NUMBER := 21;
    RESIDUO NUMBER := 0;
    TIPO_PRODUCTO CHAR(1);
    
BEGIN
    -- La funci�n MOD se encarga de obtener el rsiduo de una divisi�n.
    RESIDUO := MOD(X,2);
    
    TIPO_PRODUCTO := 'E';

    IF(RESIDUO = 0) THEN
        DBMS_OUTPUT.PUT_LINE(X || ': es par');
    ELSIF (RESIDUO > 0) THEN
        DBMS_OUTPUT.PUT_LINE(X || ': es impar');
    END IF;
    
    IF TIPO_PRODUCTO = 'A' THEN
        DBMS_OUTPUT.PUT_LINE('A --> Electr�nica');
    ELSIF TIPO_PRODUCTO = 'B' THEN
        DBMS_OUTPUT.PUT_LINE('B --> Inform�tica');
    ELSIF TIPO_PRODUCTO = 'C' THEN
        DBMS_OUTPUT.PUT_LINE('C --> Ropa');
    ELSIF TIPO_PRODUCTO = 'D' THEN
        DBMS_OUTPUT.PUT_LINE('D --> M�sica');
    ELSIF TIPO_PRODUCTO = 'E' THEN
        DBMS_OUTPUT.PUT_LINE('E --> Libros');
    ELSE
        DBMS_OUTPUT.PUT_LINE('El c�digo es incorrecto');
    END IF;

END;
