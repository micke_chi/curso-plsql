/*
Vamos a crear la tabla de multiplicar del 1 al 10, con los tres tipos de bucles: LOOP, WHILE y FOR
*/

-- Con el LOOP
/*
SET SERVEROUTPUT ON

DECLARE
    i PLS_INTEGER := 1;
    j PLS_INTEGER := 1;

BEGIN
    <<parent>>
    LOOP 
        <<child>>
        LOOP 
            DBMS_OUTPUT.PUT_LINE(i || ' x ' || j || ' = ' || (i * j));
            j := j+1; 
            EXIT child WHEN  j > 10;
        END LOOP child;
        i := i + 1; 
        j := 1;
        EXIT parent WHEN  i > 10; 
    END LOOP parent;
END;
*/

-- Con el WHILE
/*
SET SERVEROUTPUT ON

DECLARE
    i PLS_INTEGER := 1;
    j PLS_INTEGER := 1;

BEGIN
    <<parent>>
    WHILE i <= 10 LOOP 
        <<child>>
        WHILE j <= 10 LOOP 
            DBMS_OUTPUT.PUT_LINE(i || ' x ' || j || ' = ' || (i * j));
            j := j+1; 
        END LOOP child;
        i := i + 1;
        j := 1;
    END LOOP parent;
END;
*/

-- Con el FOR

SET SERVEROUTPUT ON

DECLARE

BEGIN
    <<parent>>
    FOR i IN 1..10 LOOP 
        <<child>>
        FOR j IN 1..10 LOOP
            DBMS_OUTPUT.PUT_LINE(i || ' x ' || j || ' = ' || (i * j));
        END LOOP child;
    END LOOP parent;
END;
