/*
Uso de insert dentro de pl/sql. Estas peticiones deben de llevar la sentencia COMMIT para persistir y ROLLBACK para 
deshacer los cambios recientes a la peticion en la BD
*/

-- Insertamos un registro en la BD
/*
DECLARE

    COL1 test.c1%TYPE;

BEGIN

    COL1 := 12;
    INSERT INTO TEST (C1, C2) VALUES (COL1, 'afsfgdf');
    -- Para asegurar la transaccion
    COMMIT;

END;
*/

-- Editar un registro en la BD
/*
DECLARE

    T TEST.C1%TYPE;

BEGIN

    T := 10;
    UPDATE TEST SET C2 = 'EDITADO' WHERE C1 = T;
    COMMIT;

END;
*/

-- Eliminar un registro en la BD
DECLARE

    T TEST.C1%TYPE;

BEGIN

    T := 10;
    DELETE FROM TEST WHERE C1 = T;
    COMMIT;

END;
/ -- Para cerrar el bloque se pone la barra diagonal