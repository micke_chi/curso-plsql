/**
*TRIGGER COMPUESTO: Pertime tener sobre un mismo objeto distintos tipos de triggers 
- Declaracion
- Before Statement
- After Statement
- Before Each Row
- After Each Row
*/

CREATE OR REPLACE TRIGGER TR_REGIONS7
FOR DELETE OR INSERT OR UPDATE ON REGIONS
COMPOUND TRIGGER
    -- Before statatement: el triger se lanza antes de las operaciones, por comando, no por fila
    BEFORE STATEMENT 
    IS 
    BEGIN
        INSERT INTO LOG_TABLE VALUES('BEFORE STATEMENT', USER);
    END BEFORE STATEMENT;
    
    -- After statatement: el triger se lanza despues de las operaciones, por comando, no por fila
    AFTER STATEMENT 
    IS 
    BEGIN
        INSERT INTO LOG_TABLE VALUES('AFTER STATEMENT', USER);
    END AFTER STATEMENT;
    
    -- Before each row: el triger se lanza antes de cada operacion, a nivel fila. Es decir se lanza antes de que cada fila sea afectada
    BEFORE EACH ROW 
    IS 
    BEGIN
        INSERT INTO LOG_TABLE VALUES('BEFORE EACH ROW', USER);
    END BEFORE EACH ROW;
    
    -- After each row: el triger se lanza despues de cada operacion, a nivel fila. Es decir se lanza despues de que cada fila fue afectada
    AFTER EACH ROW 
    IS 
    BEGIN
        INSERT INTO LOG_TABLE VALUES('AFTER EACH ROW', USER);
    END AFTER EACH ROW;
    
END TR_REGIONS7;
/

--INSERT INTO "HR"."REGIONS" (REGION_ID, REGION_NAME) VALUES (134, 'PARAGUAY');
UPDATE HR.REGIONS SET REGION_NAME = LOWER(REGION_NAME);


TRUNCATE TABLE LOG_TABLE;