DECLARE
  done  BOOLEAN := FALSE;
  X NUMBER:=0;
BEGIN
  --El loop finaliza cuando x < 10
  WHILE X <10 LOOP
    DBMS_OUTPUT.PUT_LINE(X);
    X:=X+1;
    --EXIT WHEN X=5;
  END LOOP;
  
  --El loop finaliza cuando el booleano es verdadero, por lo tanto solo se ejcuta una vez
  WHILE done LOOP
    DBMS_OUTPUT.PUT_LINE ('No imprimas esto.');
    done := TRUE;  
  END LOOP;

  -- El loop finaliza cundo el booleano es falso, por lo tanto el loop solo se ejecuta una vez
  WHILE NOT done LOOP
    DBMS_OUTPUT.PUT_LINE ('He pasado por aqu�');
    done := TRUE;
  END LOOP;
END;
/