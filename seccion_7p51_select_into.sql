/*
Practica1: Crear un bloque PL/SQL que devuelva al salario m�ximo del departamento 100 y lo deje  en una 
variable denominada salario_maximo y la visualice

Practica2: Visualizar el tipo de trabajo del empleado n�mero 100

Practica 3: Crear una variable de tipo DEPARTMENT_ID y ponerla alg�n valor, por ejemplo 10. Visualizar el nombre de ese
departamento y el n�mero de empleados que tiene, poniendo. Crear dos variables para albergar los valores.

Practica4: Mediante dos consultas recuperar el salario m�ximo y el salario m�nimo de la empresa e indicar su diferencia
*/

SET SERVEROUTPUT ON

DECLARE

    SALARIO_MAXIMO hr.employees.salary%TYPE;
    SALARIO_MINIMO hr.employees.salary%TYPE;
    DIFERENCIA NUMBER;
    
    TIPO_TRABAJO employees.job_id%TYPE;
    
    DEPARTMENT_ID NUMBER := 50;
    NOMBRE_DEPARTAMENTO VARCHAR2(100);
    EMPLEADOS_DEPARTAMENTO NUMBER;
    

BEGIN
    -- Obtenemos el salario m�s altos de la tabla de empleados 
    SELECT MAX(salary) as salario_maximo INTO SALARIO_MAXIMO FROM  hr.employees WHERE department_id = 100;
    DBMS_OUTPUT.PUT_LINE('El salario maximo: ' || SALARIO_MAXIMO);
    
    -- Obtenemos el tipo de trabajo del empleado 100
    SELECT job_id INTO TIPO_TRABAJO FROM  hr.employees WHERE employee_id = 100;
    DBMS_OUTPUT.PUT_LINE('El tipo de trabajo: ' || TIPO_TRABAJO);
    
    -- Obtenemos un departamento, visualizamos su nombre y la cantidad de empleados con este puesto
    SELECT d.department_name as nombre_departamento, COUNT(*) AS empleados_departamento 
    INTO NOMBRE_DEPARTAMENTO, EMPLEADOS_DEPARTAMENTO
    FROM  hr.employees e 
    JOIN hr.departments d ON e.department_id = d.department_id 
    WHERE e.department_id = 50 
    GROUP BY d.department_name;
    DBMS_OUTPUT.PUT_LINE('Nombre departamento: ' || NOMBRE_DEPARTAMENTO || ', Numero empleados: ' || EMPLEADOS_DEPARTAMENTO);
    
    --Obtener la diferencia entre el salario maximo y salario minimo de la empresa
    SELECT MAX(salary) salario_maximo, MIN(salary) salario_minimo, (MAX(salary) - MIN(salary)) diferencia 
    INTO SALARIO_MAXIMO, SALARIO_MINIMO, DIFERENCIA
    FROM  hr.employees;
    
    DBMS_OUTPUT.PUT_LINE('La diferencia entre el salario max: ' || SALARIO_MAXIMO || ' y el salario min: ' || SALARIO_MINIMO || ' es de: ' || DIFERENCIA);
    
END;
