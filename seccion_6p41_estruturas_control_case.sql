SET SERVEROUTPUT ON

DECLARE
    USUARIO VARCHAR2(40);
    
BEGIN
    USUARIO := USER;
    
    DBMS_OUTPUT.PUT_LINE('El usuario es: ' || USUARIO);
    
    CASE USUARIO
        WHEN 'SYS' THEN DBMS_OUTPUT.PUT_LINE('Eres superadministrador');
        WHEN 'SYSTEM'  THEN DBMS_OUTPUT.PUT_LINE('Eres administrador');
        WHEN 'HR'  THEN DBMS_OUTPUT.PUT_LINE('Eres recursos humanos');
        ELSE DBMS_OUTPUT.PUT_LINE('Usuario no autorizado');
    END CASE;

END;