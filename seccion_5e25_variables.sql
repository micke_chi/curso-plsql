-- VARIABLES
-- Existe el tipado de varibales, pueden ser usadas en comandos SQL
-- Las variables deben comenzar con una letra, pueden tener carcateres especiales y numeros
-- La longitud del nombre de las variables no puede rebasarlos 30 caracteres
-- Se declaran e inicializan en la seccion DECLARE del bloque PLSQL
-- Las variables pueden pasarse como parametros a otros programas PLSQL y puden almacenar valores de otros programas PLSQL
-- Para m�s detalles de tipos de variables http://epnbdd-oracle.blogspot.com/2012/05/tipos-de-datos-en-oracle.html

-- Como declaramos variables entonces agregamos la zona DECLARE
DECLARE
    SALARY NUMBER(10) := 1000;
    NAME VARCHAR2(100) := 'Micke chi';
    BIRTH_DAY DATE := '2018-12-14';
    ACTIVO BOOLEAN := TRUE;

-- En la zona BEGIN se pueden hacer las operaciones qque necesitemos
BEGIN
    SALARY := SALARY*60;
    DBMS_OUTPUT.PUT_LINE('Mi nombre es: ' ||NAME);
    DBMS_OUTPUT.PUT_LINE('Mi salario es: ' ||SALARY);

    IF ACTIVO THEN
        DBMS_OUTPUT.PUT_LINE('Su estatus es ACTIVO');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Su estatus es INACTIVO');
    END IF;

END;
