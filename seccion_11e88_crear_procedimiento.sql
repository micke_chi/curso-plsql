/*
- Bloques anonimos: Son todos aquellos bloques con los que hemos trabajado hasta el momento. 
    - Hay que guardarlos en un archivo .sql
    - Son independientes de la BD
    - No tienen nombre
    - Cada vez que se ejecutan hay que cargarlos, compilarlos y ejecutarlos

- Procedimientos y funciones almacenadas: tienen una caracter�stica en comun, se guardan en la BD compilados y se pueden 
reutilizar en cualquier momento. tienen un nombre y otras caracteristicas:
    - Ejemplos de estos:
        - Procedures
        - Functions
        - Packajes
        - Triggers
    - Son m�s rapidos que los bloques an�nimos:
        - Al crear el objeto se guarda en la bd el codigo fuente y el pseudo codigo compilado.
        - Se pueden invocar en cualquier momento
*/

CREATE OR REPLACE PROCEDURE PROCS1 
(
  P1 IN VARCHAR2 
) AS 
BEGIN
  DBMS_OUTPUT.PUT_LINE('ESTE ES EL PRIMER PROCEDIMIENTOS');
-- Es recomendable terminar el procedimiento con el nombre del mismo
END PROCS1;


-- Los procedimientos pueden ser ejecutados mediante la sentencia EXECUTE 'NOMBRE_DEL_PROCEDIMEINTO'
EXECUTE PROCS1(1);